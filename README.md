# HTML Static Boilerplate 
---

## Duplicate this repo
```
git clone git@gitlab.com:LaurentRoy/static-dev.git new-project # or git clone https://gitlab.com/LaurentRoy/static-dev.git
cd new-project
git remote rename origin static-html
git remote add origin git@gitlab.com:LaurentRoy/new-project.git
git push -u origin --all
```

## Install NPM Library
'npm install'

## Running  dev environment
. in our path
	. open terminal
	. run 'gulp' or 'npm run dev'

## Prepare production
```
npm run build
```

## Useful links
[Hamburger CSS](https://jonsuh.com/hamburgers/)
[Optimizing Google Fonts Performance](https://www.smashingmagazine.com/2019/06/optimizing-google-fonts-performance/)
[Undesign](https://undesign.learn.uno/) - Collection of free design tools and resources for makers, developers and designers
[Unminus](https://www.unminus.com/) - Free Premium music
[Static Site Boilerplate](https://github.com/ericalli/static-site-boilerplate) - A better workflow for building modern static websites. 
[Front-End Performance Checklist](https://github.com/thedaviddias/Front-End-Performance-Checklist) - The only Front-End Performance Checklist that runs faster than the others
[UIkit](https://getuikit.com/docs/accordion) - Perfect UI kit for Web App project
[Pixelify](https://pixelify.net/) - Best Free Fonts, Mockups and Graphics - Updated Daily
[heml](https://heml.io/) - Quickly craft clean, responsive emails
[imask.js](https://unmanner.github.io/imaskjs/) - vanilla javascript input mask
[critical CSS](https://github.com/addyosmani/critical) - Extract & Inline Critical-path CSS in HTML pages
[JSON placeholder](https://jsonplaceholder.typicode.com/) - Fake Online REST API for Testing and Prototyping
[Frappe](https://frappe.io/charts) - Modern, Open Source SVG Charts
[icons8](https://icons8.com/animated-icons) - 200 free animated icons

## Changelog
* 28 September, 2019
	* v0.1
* 02 Octobre, 2019
	* v0.1.1
		* ajout du lien du dépot en https
* 13 Novembre, 2019
	* v0.1.2
		* maj de la structure (scss/pug)
* 2 Mars, 2019
	* v0.2
		* Changement du scss par du sass
		* Custom Bootstrap
		* Mise à jour du gulp pour avoir un main.sass automatique
		* Sass auto import in directory
		* Utilisation de la notion de module plutôt que de component
		* Navbar par défaut
		* Added useful Links




![Merci](http://wearemerci.com/screenshot.png)